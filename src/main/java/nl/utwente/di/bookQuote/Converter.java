package nl.utwente.di.bookQuote;

public class Converter {

    public double converter(double temp) {
        if(temp * 1.8 + 32 != temp) {
            return temp * 1.8 + 32;
        }
        else {
            return (temp - 32) * 0.5556;
        }
    }
}
